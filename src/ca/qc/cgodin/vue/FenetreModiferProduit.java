package ca.qc.cgodin.vue;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

public class FenetreModiferProduit extends JInternalFrame{

	public FenetreModiferProduit() {
		
//		Une fenetre qui donne deux options modifier un livre ou un film
	    super("Modifier produit", true, true, true, true);
	    setSize(700, 700);
	    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	    // JDesktopPane
	    JDesktopPane desktop = new JDesktopPane();
	    
	    // Creer buttons
	    JButton btnLivre = new JButton("Livre");
	    JButton btnFilm = new JButton("Film");

	    JPanel panel = new JPanel(new GridBagLayout());
	    
	
	    GridBagConstraints gbc = new GridBagConstraints();

	    // ajout des contraintes et btn 
	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    gbc.insets = new Insets(5, 5, 5, 5);
	    btnLivre.addActionListener(new ActionListener() {
	        
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            FenetreModifierLivre f = new FenetreModifierLivre();
	            
	            desktop.add(f); 
	        }
	        
	    });
	    panel.add(btnLivre, gbc);

	 // ajout des contraintes et btn 

	    gbc.gridx = 1;
	    btnFilm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreModifierFilm f = new FenetreModifierFilm();
	            
	            desktop.add(f); 
			}
		});
	    panel.add(btnFilm, gbc);

	    add(panel, BorderLayout.NORTH); 

	    add(desktop, BorderLayout.CENTER);

	    setVisible(true);
	}

	
}
