package ca.qc.cgodin.vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.Film;
import ca.qc.cgodin.modele.Livre;

import ca.qc.cgodin.modele.Livre;

public class FenetreNouveauFilm extends JInternalFrame{
	private JLabel titreLbl= new JLabel("Titre");
	private JLabel imageLbl= new JLabel("Image");
	private JLabel prixAchatLbl= new JLabel("Prix d'achat");
	private JLabel prixVenteLbl= new JLabel("Prix de vente");
	private JLabel etatLbl= new JLabel("Etat");
	private JLabel utilisation= new JLabel("Utilisation");
	private JLabel labelRealisateur = new JLabel("Realisateur");
	private JTextField titreTxt = new JTextField(25);
	private JTextField imageTxt = new JTextField(25);
	private JTextField prixAchatTxt = new JTextField(25);
	private JTextField prixVenteTxt = new JTextField(25);
	private JTextField etatTxt = new JTextField(25);
	private JTextField utilTxt = new JTextField(25);
	
	private JTextField tfRealisateur = new JTextField(25);
	private JLabel labelStudio = new JLabel("Studio");
	private JTextField tfStudio = new JTextField(25);
	private JLabel labelFormat = new JLabel("Format");
	private JTextField tfFormat = new JTextField(25);
	private JLabel labelActeurs = new JLabel("Acteurs");
	private JTextField tfActeurs = new JTextField(25);
	private JLabel labelLanguePisteSonore = new JLabel("Langue piste sonore");
	private JTextField tfLanguePisteSonore = new JTextField(25);
//	private JLabel labelISBN = new JLabel("ISBN");
//	private JTextField tfISBN = new JTextField(25);
	private JPanel imagePanel = new JPanel();
	private JLabel imageLabel = new JLabel();
	
	
	private JPanel panelCardLayout;
	private CardLayout layout;
//	Constructeur
	public FenetreNouveauFilm() {
		super("Nouveau Film");
		setSize(500, 600);
		setClosable(true);
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		add(getPanelCentre(), BorderLayout.CENTER);
		add(getPanelHaut(), BorderLayout.NORTH);
		  add(imagePanel, BorderLayout.SOUTH);
		setVisible(true);

	}
	
//	Contenu du centre
	public JPanel getPanelCentre() {
	    panelCardLayout = new JPanel();
	    layout = new CardLayout();
	    panelCardLayout.setLayout(layout);

	    // Card1
	    JPanel card1 = new JPanel(new GridBagLayout());
	    GridBagConstraints g = new GridBagConstraints();

	    g.gridx = 0;
	    g.gridy = 0;
	    card1.add(new JLabel("Titre"), g);

	    g.gridx = 1;
	    g.gridy = 0;
	    card1.add(titreTxt, g);

	    g.gridx = 0;
	    g.gridy = 1;
	    card1.add(new JLabel("Image"), g);

	    g.gridx = 1;
	    g.gridy = 1;
	    card1.add(imageTxt, g);

	    g.gridx = 0;
	    g.gridy = 2;
	    card1.add(new JLabel("Prix d'achat"), g);

	    g.gridx = 1;
	    g.gridy = 2;
	    card1.add(prixAchatTxt, g);

	    g.gridx = 0;
	    g.gridy = 3;
	    card1.add(new JLabel("Prix de vente"), g);

	    g.gridx = 1;
	    g.gridy = 3;
	    card1.add(prixVenteTxt, g);

	    g.gridx = 0;
	    g.gridy = 4;
	    card1.add(new JLabel("Etat"), g);

	    g.gridx = 1;
	    g.gridy = 4;
	    card1.add(etatTxt, g);

	    g.gridx = 0;
	    g.gridy = 5;
	    card1.add(new JLabel("Utilisation"), g);

	    g.gridx = 1;
	    g.gridy = 5;
	    card1.add(utilTxt, g);

	    // Card2
	    JPanel card2 = new JPanel(new GridBagLayout());
	    g = new GridBagConstraints();

	    Insets insets = new Insets(5, 10, 5, 10);

	    g.insets = insets;

	    g.gridx = 0;
	    g.gridy = 0;
	    card2.add(new JLabel("Réalisateur"), g);

	    g.gridx = 1;
	    g.gridy = 0;
	    g.weightx = 1.0;
	    g.fill = GridBagConstraints.HORIZONTAL;
	    card2.add(tfRealisateur, g);

	    g.gridx = 0;
	    g.gridy = 1;
	    card2.add(labelStudio, g);

	    g.gridx = 1;
	    g.gridy = 1;
	    g.weightx = 1.0;
	    g.fill = GridBagConstraints.HORIZONTAL;
	    card2.add(tfStudio, g);

	    g.gridx = 0;
	    g.gridy = 2;
	    card2.add(labelFormat, g);

	    g.gridx = 1;
	    g.gridy = 2;
	    g.weightx = 1.0;
	    g.fill = GridBagConstraints.HORIZONTAL;
	    card2.add(tfFormat, g);

	    g.gridx = 0;
	    g.gridy = 3;
	    card2.add(labelActeurs, g);

	    g.gridx = 1;
	    g.gridy = 3;
	    g.weightx = 1.0;
	    g.fill = GridBagConstraints.HORIZONTAL;
	    card2.add(tfActeurs, g);

	    g.gridx = 0;
	    g.gridy = 4;
	    card2.add(labelLanguePisteSonore, g);

	    g.gridx = 1;
	    g.gridy = 4;
	    g.weightx = 1.0;
	    g.fill = GridBagConstraints.HORIZONTAL;
	    card2.add(tfLanguePisteSonore, g);

	    g.insets = new Insets(0, 0, 0, 0);
	    g.weightx = 0.0;
	    g.fill = GridBagConstraints.NONE;

	    panelCardLayout.add(card1, "Card1");
	    panelCardLayout.add(card2, "Card2");
	    layout.show(panelCardLayout, "Card1");

	    return panelCardLayout;
	}


	
	
//	Contenu du haut
	public JPanel getPanelHaut() {
		JPanel panelHaut = new JPanel();
		JButton cmdPrecedent = new JButton("Page 1");
		cmdPrecedent.setEnabled(false);
		JButton cmdSuivant = new JButton("Page 2");
		JButton cmdEnregistrer = new JButton("Enregistrer");
		JButton cmdAnnuler = new JButton("Annuler");
		panelHaut.add(cmdPrecedent);
		panelHaut.add(cmdSuivant);
		panelHaut.add(cmdEnregistrer);
		panelHaut.add(cmdAnnuler);

		cmdPrecedent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				layout.first(panelCardLayout);
				cmdPrecedent.setEnabled(false);
				cmdSuivant.setEnabled(true);

			}
		});
		cmdSuivant.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				layout.last(panelCardLayout);
				cmdPrecedent.setEnabled(true);
				cmdSuivant.setEnabled(false);
			}
		});
		cmdEnregistrer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				enregistrer();
			}
		});
		cmdAnnuler.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nettoyer();
			}
		});
		return panelHaut;
	}
	

// Eregistrer le nouveau film
	private void enregistrer() {
	    String titre = titreTxt.getText();
	    String image = imageTxt.getText();
	    String prixAchatStr = prixAchatTxt.getText();
	    String prixVenteStr = prixVenteTxt.getText();
	    String etat = etatTxt.getText();
	    String utilisation = utilTxt.getText();

	    String realisateur = tfRealisateur.getText();
	    String studio = tfStudio.getText();
	    String format = tfFormat.getText();
	    String acteurs = tfActeurs.getText();
	    String langue = tfLanguePisteSonore.getText();
	    
//	    Valider les infos ecrites par l'utilisateur
	    if (titre.isEmpty() || image.isEmpty() || etat.isEmpty() || utilisation.isEmpty() || realisateur.isEmpty()
	            || studio.isEmpty() || format.isEmpty() || acteurs.isEmpty() || langue.isEmpty()
	            || prixAchatStr.isEmpty() || prixVenteStr.isEmpty()) {
	        JOptionPane.showMessageDialog(this, "Il faut remplir le formulaire", "Information manquante",
	                JOptionPane.WARNING_MESSAGE);
	        return;
	    }
	    
	    if (!etat.equalsIgnoreCase("disponible")) {
	        JOptionPane.showMessageDialog(this, 
	            "Pour enregistrer un film, l'etat doit etre \"disponible\".",
	            "Input Error",
	            JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    if (utilisation.equalsIgnoreCase("a emprunte") && !prixVenteStr.equals("0")) {
	        JOptionPane.showMessageDialog(null, "Ce produit est a emprunter, le prix de vente doit être 0. Veuillez entrer 0 comme prix de vente.");
	        return; 
	    }
	   

	    
	    if (!(utilisation.equalsIgnoreCase("a vendre") || utilisation.equalsIgnoreCase("a emprunte"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Information invalide pour \"utilisation\", veuillez ecrire  \"a vendre\" ou \"a emprunte\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	    if (!(format.equalsIgnoreCase("DVD") || format.equalsIgnoreCase("Blue Ray"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Info invalide pour \"format\". veuillez ecrire  \"DVD\" ou \"Blue Ray\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	    if (!(langue.equalsIgnoreCase("francais") || langue.equalsIgnoreCase("anglais"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Info invalide pour\"langue piste sonore\". veuillez ecrire  \"anglais\" ou \"francais\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	

	    
	   
//	    Verifie si les infos sont acceptables selon la BD
	    try {
	        double prixAchat = Double.parseDouble(prixAchatStr);
	        double prixVente = Double.parseDouble(prixVenteStr);
	        
	        if (utilisation.equalsIgnoreCase("a vendre")) {
	        	if (prixVente != prixAchat * 0.3) {
		            JOptionPane.showMessageDialog(this, 
		                  "Le prix de vente doit être 30% du prix d'achat.", 
		                  "Erreur de saisie", 
		                  JOptionPane.ERROR_MESSAGE);
		            return;
		        }
			}
	        
	        
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	     
	        
	        Film f1 = new Film(titre, image, prixAchat, prixVente, etat, utilisation, realisateur, studio, format,
	                acteurs, langue);  
	        f1.enregistrer();
	        JOptionPane.showMessageDialog(this,
	                "Un film vient d'etre enregistre. \nSon code est : " + f1.getCode(), "Livre enregistre",
	                JOptionPane.PLAIN_MESSAGE);
	        nettoyer();
	    } catch (NumberFormatException e) {
	        JOptionPane.showMessageDialog(this,
	                "Assurez vous de rentrer un prix en chiffre.", "Erreur de saisie", JOptionPane.ERROR_MESSAGE);
	    } catch (SQLException ex) {
	        JOptionPane.showMessageDialog(this,
	                "Un probleme existe dans le systeme, veuillez contacter\nl'administrateur du systeme",
	                "Erreur Systeme", JOptionPane.ERROR_MESSAGE);
	        System.out.println(ex.getMessage());
	    }
	    
//	    Affiche l'image
	    if (!image.isEmpty()) {
            try {
            	
    			
            	   String imagePath = "ressource" + File.separator + image; 
            	    BufferedImage img= ImageIO.read(new File(imagePath));
            	    ImageIcon icon = new ImageIcon(img);
            	    imageLabel.setIcon(icon);
            	    imagePanel.revalidate();
            	    imagePanel.repaint();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "l'image n'a pas pu etre charge", "Image Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
	
	


	
	private void nettoyer() {

		titreTxt.setText("");
		imageTxt.setText("");
		prixAchatTxt.setText("");
		prixVenteTxt.setText("");
		etatTxt.setText("");
		utilTxt.setText("");
		tfRealisateur.setText("");
		tfStudio.setText("");
		tfFormat.setText("");
		tfActeurs.setText("");
		tfLanguePisteSonore.setText("");
		
	

	}
}


