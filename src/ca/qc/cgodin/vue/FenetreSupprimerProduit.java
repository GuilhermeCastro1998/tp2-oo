package ca.qc.cgodin.vue;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;

// Offre deux optios pour supprimer un livre ou un film
public class FenetreSupprimerProduit extends JInternalFrame{

	public FenetreSupprimerProduit() {
	    super("Supprimer produit", true, true, true, true);
	    setSize(700, 700);
	    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	    JDesktopPane desktop = new JDesktopPane();
	    
	    JButton btnLivre = new JButton("Livre");
	    JButton btnFilm = new JButton("Film");

	    JPanel panel = new JPanel(new GridBagLayout());
	    
	    GridBagConstraints gbc = new GridBagConstraints();

	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    gbc.insets = new Insets(5, 5, 5, 5);
	    btnLivre.addActionListener(new ActionListener() {
	        
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            FenetreSupprimerLivre f = new FenetreSupprimerLivre();
	            
	            desktop.add(f); 
	        }
	        
	    });
	    panel.add(btnLivre, gbc);

	    gbc.gridx = 1;
	    btnFilm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreSupprimerFilm f = new FenetreSupprimerFilm();
	            
	            desktop.add(f); 
			}
		});
	    panel.add(btnFilm, gbc);

	    add(panel, BorderLayout.NORTH); 

	    add(desktop, BorderLayout.CENTER);

	    setVisible(true);
	}
}
