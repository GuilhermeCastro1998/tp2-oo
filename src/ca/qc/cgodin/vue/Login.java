package ca.qc.cgodin.vue;

import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.DBManager;

public class Login extends JFrame {

	private JTextField txtNom;
	private JTextField txtMotDePAsse;

	public Login() {
		setTitle("Connexion");
		setSize(400, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(getPanelCentre());
		setResizable(false);
		setVisible(true);
	}

//	Contenu du centre
	private JPanel getPanelCentre() {
		JPanel panel = new JPanel(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;

		Font labelFont = new Font(Font.SANS_SERIF, Font.BOLD, 16);

		JLabel lblNom = new JLabel("Nom d'utilisateur");
		lblNom.setFont(labelFont);
		panel.add(lblNom, gbc);

		gbc.gridx = 1;
		txtNom = new JTextField(15);
		panel.add(txtNom, gbc);

		gbc.gridy = 1;
		gbc.gridx = 0;

		JLabel lblMotDePasse = new JLabel("Mot de passe");
		lblMotDePasse.setFont(labelFont);
		panel.add(lblMotDePasse, gbc);
		gbc.gridx = 1;
		txtMotDePAsse = new JPasswordField(15);
		panel.add(txtMotDePAsse, gbc);

		   gbc.gridy = 2;
		    gbc.gridwidth = 1;  
		    gbc.anchor = GridBagConstraints.CENTER;
		    gbc.insets.top = 10;

		 
		    JButton annulerBtn = new JButton("Annuler");
		    annulerBtn.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					nettoyer();
				}
			});
		    gbc.gridx = 0; 
		    panel.add(annulerBtn, gbc);

		 
		    JButton connecterBtn = new JButton("Connexion");
		    gbc.gridx = 1; 
		    gbc.insets.bottom = 10; 
		    connecterBtn.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					connecter();
					
				}
			});
		    panel.add(connecterBtn, gbc);

		return panel;
	}

//	connecter le prepose selon les infos acceptables dans la base de donnee
	private void connecter() {
		String username = txtNom.getText();
		String pass = txtMotDePAsse.getText();
		
		 try {
		        String requete = "SELECT * FROM prepose WHERE username = '" +username+ "'AND password = '"+ pass+"'";
		        

		        ResultSet res = DBManager.executeQuery(requete);

		        if (res.next()) {
		            JOptionPane.showMessageDialog(this, "Connection reussie");
		            new MenuPrincipal();
		            dispose();
		        } else {
		            
		            JOptionPane.showMessageDialog(this, "Erreur dans le nom d'utilisateur ou mot de passe");
		        }
		    } catch (SQLException e) {
		        e.printStackTrace();
		        JOptionPane.showMessageDialog(null, "Erreur dans la connnection");
		    }
	}
	
	
	private void nettoyer() {
		txtNom.setText("");
		txtMotDePAsse.setText("");

	}
	
	

}
