package ca.qc.cgodin.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import ca.qc.cgodin.modele.DBManager;

public class MenuPrincipal extends JFrame {
	JMenu menuClient;
	JMenu menuPret;
	JMenu menuProduit;
	JMenuItem optNvClient;
	JMenuItem optNvProduit;
	JMenuItem optModifierProduit;
	JMenuItem optSupprimerProduit;
	JMenuItem optInfosClient;
	JMenuItem optListerLivre;
	JMenuItem optPret;
	JMenuItem optListePrets;
	private JDesktopPane desktop;
// Menu principal qui donne acces a toutes les autres fenetres
	public MenuPrincipal()  {
		super("Menu principal");
		setBounds(0, 0, 1000, 1000);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBackground(Color.red);
		ajouterBarreMenu();
		creerToolBar();
		desktop = new JDesktopPane();
		setContentPane(desktop);
		JPanel contentPane = new JPanel(new BorderLayout()); 
		contentPane.add(creerToolBar(), BorderLayout.PAGE_START); 
		contentPane.add(desktop, BorderLayout.CENTER);

		setContentPane(contentPane); 

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				// Fermeture de connection
				try {
					DBManager.fermerConnection();
					JOptionPane.showMessageDialog(MenuPrincipal.this, "Connection BD ferme");
					System.exit(0);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					System.out.println(e1.getMessage());
				}

			}
		});
		setVisible(true);
	}
	
//	Crestion der la barre menu
	public void ajouterBarreMenu() {
		JMenuBar bar = new JMenuBar();
		menuClient = new JMenu("Client");
		optNvClient = new JMenuItem("Nouveau");
		optNvClient.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
				FenetreNouveauClient f = new FenetreNouveauClient();
				desktop.add(f);
				desktop.setSelectedFrame(f);
				
			}
		});
		menuClient.add(optNvClient);
		optInfosClient = new JMenuItem("Rechercher Client");
		optInfosClient.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreRecherClients f = new FenetreRecherClients();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
		menuClient.add(optInfosClient);
		
		menuProduit = new JMenu("Produit");
		optNvProduit = new JMenuItem("Nouveau");
		optNvProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
//				FenetreNouveauProduit f = new FenetreNouveauProduit();
				FenetreNouveauProduit f = new FenetreNouveauProduit();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
		menuProduit.add(optNvProduit);
		
		optModifierProduit = new JMenuItem("Modifier");
		optModifierProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreModiferProduit f = new FenetreModiferProduit();
				desktop.add(f);
			desktop.setSelectedFrame(f);
				
			}
		});
		menuProduit.add(optModifierProduit);
		
		optSupprimerProduit = new JMenuItem("Supprimer");
		optSupprimerProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreSupprimerProduit f = new FenetreSupprimerProduit();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
		menuProduit.add(optSupprimerProduit);
		
		optListerLivre = new JMenuItem("afficher les livres");
		optListerLivre.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FenetreAfficherLivres f = new FenetreAfficherLivres();
				desktop.add(f);
				desktop.setSelectedFrame(f);
				
			}
		});
		menuProduit.add(optListerLivre);
		
		
		
		menuPret = new JMenu("Pret");
		optPret = new JMenuItem("Effectuer un pret");
		optPret.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FenetrePret f = new FenetrePret();
				desktop.add(f);
				desktop.setSelectedFrame(f);
				
			}
		});
		menuPret.add(optPret);
		
		optListePrets = new JMenuItem("Afficher tous les prets");
		optListePrets.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreListePret f = new FenetreListePret();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
		menuPret.add(optListePrets);
		
		
		bar.add(menuClient);
		bar.add(menuProduit);
		bar.add(menuPret);
		setJMenuBar(bar);
	}
	
//	Creation de la tool bar
	
	private JToolBar creerToolBar() {
		JToolBar toolBar = new JToolBar();
		
		JButton btnNew = new JButton( new ImageIcon( "icons" + File.separator +   "new.png") );
        btnNew.setToolTipText( "Nouveau Client" );
        btnNew.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreNouveauClient f = new FenetreNouveauClient();
				desktop.add(f);
				desktop.setSelectedFrame(f);
				
			}
		} );
        toolBar.add( btnNew );
        
        JButton btnAfficher = new JButton( new ImageIcon( "icons" + File.separator + "about.png") );
        btnAfficher.setToolTipText( "Rechercher Infos Client" );
        btnAfficher.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreRecherClients f = new FenetreRecherClients();
				desktop.add(f);
				desktop.setSelectedFrame(f);
				
			}
		} );
        toolBar.add( btnAfficher );
        
        JButton btnNvProduit = new JButton(new ImageIcon("icons" + File.separator + "nvProd.png"));
        btnNvProduit.setToolTipText( "Nouveau Produit" );
        btnNvProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreNouveauProduit f = new FenetreNouveauProduit();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
        toolBar.add(btnNvProduit);
        
        JButton btnModProduit = new JButton(new ImageIcon("icons" + File.separator + "redo.png"));
        btnModProduit.setToolTipText( "Modifier produit" );
        btnModProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreModiferProduit f = new FenetreModiferProduit();
				desktop.add(f);
			desktop.setSelectedFrame(f);
			}
		});
        toolBar.add(btnModProduit);
        
        JButton btnSupprimerProduit = new JButton(new ImageIcon("icons" + File.separator + "exit.png"));
        btnSupprimerProduit.setToolTipText( "Supprimer produit" );
        btnSupprimerProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreSupprimerProduit f = new FenetreSupprimerProduit();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
        toolBar.add(btnSupprimerProduit);
        
        JButton btnAfficherLivresProduit = new JButton(new ImageIcon("icons" + File.separator + "pile-de-livres-2.png"));
        btnAfficherLivresProduit.setToolTipText( "Afficher les livres" );
        btnAfficherLivresProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FenetreAfficherLivres f = new FenetreAfficherLivres();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
        toolBar.add(btnAfficherLivresProduit);
        
        JButton btnPretProduit = new JButton(new ImageIcon("icons" + File.separator + "achat.png"));
        btnPretProduit.setToolTipText( "Effectuer un pret" );
        btnPretProduit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FenetrePret f = new FenetrePret();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
        toolBar.add(btnPretProduit);
        
        JButton btnAfficherPrets = new JButton(new ImageIcon("icons" + File.separator + "affiche-2.png"));
        btnAfficherPrets.setToolTipText( "Afficher tous les prets" );
        btnAfficherPrets.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FenetreListePret f = new FenetreListePret();
				desktop.add(f);
				desktop.setSelectedFrame(f);
			}
		});
        toolBar.add(btnAfficherPrets);
        
        
        return toolBar;
	}
	
	
	

}
