package ca.qc.cgodin.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import ca.qc.cgodin.modele.BiblioException;
import ca.qc.cgodin.modele.Client;
import ca.qc.cgodin.modele.Pret;

public class FenetreListePret extends JInternalFrame {
	
//	private JTextField txtID;
//	private JTextField codeClient;
//	private JTextField codeProduit;
//	private JTextField date;
	

	String [] titreColonne = {"ID produit","Code du client", "Code du Produit", "Date de pret"};
	String [][] donnees = new String [15][titreColonne.length];
	JTable table;
	
	public FenetreListePret() {
		super("Tous les prets", true,true,true,true);
		setSize(800,600);
		JPanel contentPane = new JPanel();
		contentPane.add(getPanelCentre());
		add(contentPane);
		setVisible(true);
	}
	
	private JPanel getPanelCentre() {
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
//		c.gridx =0;
//		c.gridy =0;
		c.insets = new Insets(5, 5, 5, 5);
		c.fill = GridBagConstraints.BELOW_BASELINE_TRAILING;
//		


	        c.gridx =1;
	        c.gridy =2;
	        JButton cmdAfficher = new JButton("AFFICHER");
	        cmdAfficher.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					afficher();
					
				}
			});
	        panel.add(cmdAfficher,c);

	        c.gridx =0;
	        c.gridy =3;
	        c.gridwidth =5;
	        c.gridheight =3;
	        table = new JTable(new ModeleTable(donnees, titreColonne));
	        panel.add(new JScrollPane(table),c);

	        return panel;

		
	
	}
	// Methode afficher qui permet de récupérer la liste des prêts et de les afficher dans la JTable
	private void afficher() {
		  try {
		        List<Pret> prets = Pret.recupererListePret();

		        // Convertir les prets en un array de 2 dimensions
		        Object[][] donnees = new Object[prets.size()][titreColonne.length];
		        for (int i = 0; i < prets.size(); i++) {
		            Pret pret = prets.get(i);
		            donnees[i][0] = pret.getId();
		            donnees[i][1] = pret.getCodeClient();
		            donnees[i][2] = pret.getCodeProduit();
		            donnees[i][3] = pret.getDatePret();
		        }

		        // Update la table
		        DefaultTableModel tableModel = new DefaultTableModel(donnees, titreColonne);
		        table.setModel(tableModel);
		    } catch (SQLException ex) {
		     
		        System.out.println(ex.getMessage());
		    }
	}
	
}
