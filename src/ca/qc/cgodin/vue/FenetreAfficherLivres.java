package ca.qc.cgodin.vue;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.Livre;

public class FenetreAfficherLivres extends JInternalFrame{

	private JTextField txtCode;
	private JTextField txtTitre;

	private JTextField txtAuteur;
	private JTextField txtEditeur;
	private JTextField txtCollection;
	private JTextField txtLangue;
	private JTextField txtDatePublication;

	// Declaration de la structure de la table qui affiche la liste des livre
	String [] titreColonne = {"Code","Titre", "Auteur", "Editeur", "Collection", "Langue","Date de publication"};
	String [][] donnees = new String [15][titreColonne.length];
	JTable table;
	public FenetreAfficherLivres() {
		super("Liste de livres", true,true,true,true);
		setSize(1000,900);
		JPanel contentPane = new JPanel();
		contentPane.add(getPanelCentre());
		add(contentPane);
		setVisible(true);
	}
	
//	Creation du panel du centre
	private JPanel getPanelCentre() {
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx =0;
		c.gridy =0;
		c.insets = new Insets(5, 5, 5, 5);
		c.fill = GridBagConstraints.BELOW_BASELINE_TRAILING;

		// Ajout des labels et des champs de texte au panneau
		panel.add(new JLabel("Code"),c);
	    c.gridx=1;
	    txtCode = new JTextField(15);
	    panel.add(txtCode,c);
		
		c.gridx =2;
	    panel.add(new JLabel("Titre"),c);
	    c.gridx=3;
	    txtTitre = new JTextField(15);
	    panel.add(txtTitre,c);

	    c.gridy =1;
	    c.gridx =0;
	    panel.add(new JLabel("Auteur"),c);
	    c.gridx=1;
	    txtAuteur = new JTextField(15);
	    panel.add(txtAuteur,c);

	    c.gridx =2;
	    panel.add(new JLabel("Editeur"),c);
	    c.gridx = 3;
	    txtEditeur = new JTextField(15);
	    panel.add(txtEditeur,c);
	    
	    c.gridy =2;
	    c.gridx =0;
	    panel.add(new JLabel("Collection"),c);
	    c.gridx = 1;
	    txtCollection = new JTextField(15);
	    panel.add(txtCollection, c);
	    
	    c.gridx =2;
	    panel.add(new JLabel("Langue"),c);
	    c.gridx = 3;
	    txtLangue = new JTextField(15);
	    panel.add(txtLangue,c);
	    
	    c.gridy =3;
	    c.gridx =0;
	    panel.add(new JLabel("Date de publication"),c);
	    c.gridx = 1;
	    txtDatePublication = new JTextField(15);
	    panel.add(txtDatePublication,c);

	    c.gridx =1;
	    c.gridy =4;
	 // Ajout d'un bouton pour afficher les livres.
	    JButton cmdAfficher = new JButton("AFFICHER");
	    cmdAfficher.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				afficherLivre();
//				Appelle la methode pour afficher les livres
			}
		});
	    panel.add(cmdAfficher,c);

	    c.gridx =0;
	    c.gridy =5;
	    c.gridwidth =5;
	    c.gridheight =3;
	    
	 // Création d'une table pour afficher la liste des livres.
	    table = new JTable(new ModeleTable(donnees, titreColonne));
	  
	    table.getColumnModel().getColumn(0).setPreferredWidth(100); 
		table.getColumnModel().getColumn(1).setPreferredWidth(100);
		table.getColumnModel().getColumn(2).setPreferredWidth(100);
		table.getColumnModel().getColumn(3).setPreferredWidth(100);
		table.getColumnModel().getColumn(4).setPreferredWidth(100);
		table.getColumnModel().getColumn(5).setPreferredWidth(100);
		table.getColumnModel().getColumn(6).setPreferredWidth(200); 
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(800, 400));
		panel.add(scrollPane, c);

	    return panel;
	}

	// Methode qui affiche les livres dans la table
	private void afficherLivre() {
		try {
			String code = txtCode.getText();
			String titre = txtTitre.getText();
			String auteur = txtAuteur.getText();
			String editeur = txtEditeur.getText();
			String collection = txtCollection.getText();
			String langue = txtLangue.getText();
			String datePublication = txtDatePublication.getText();
			ArrayList<Livre> listeLivre = new ArrayList<Livre>(); // Création d'une liste pour stocker les livres
			
			// Vérifier si tous les champs de texte sont vides.
			if (code.equals("") && titre.equals("") && auteur.equals("") && editeur.equals("") && collection.equals("") && langue.equals("") && datePublication.equals("")) {
				listeLivre = Livre.recupererListeLivre();
			} else {
//				 créer un HashMap pour stocker les critères de recherche
				HashMap<String, String> criteres = new HashMap<String, String>();
			
				if (!code.equals("")) {
					criteres.put("isbn", code);
				}
				if (!titre.equals("")) {
					criteres.put("titre", titre);
				}
				if (!auteur.equals("")) {
					criteres.put("auteur", auteur);
				}
				if (!editeur.equals("")) {
					criteres.put("editeur", editeur);
				}
				if (!collection.equals("")) {
					criteres.put("collection", collection);
				}
				if (!langue.equals("")) {
					criteres.put("langue", langue);
				}
				if (!datePublication.equals("")) {
					criteres.put("datePublication", datePublication);
				}
				
				// Récupérer la liste des livres en fonction des critères de recherche.
				listeLivre = Livre.recupererListeLivre(criteres);
			}

			// Création d'un nouveau tableau pour stocker les données des livres.
			donnees = new String[listeLivre.size()][7];
			int i = 0;
			
			// Parcourir la liste des livres et ajouter chaque livre au tableau.
			for (Livre lv : listeLivre) {
				donnees[i][0] = lv.getCode();
				donnees[i][1] = lv.getTitre();
				donnees[i][2] = lv.getAuteur();
				donnees[i][3] = lv.getEditeur();
				donnees[i][4] = lv.getCollection();
				donnees[i][5] = lv.getLangue();
				donnees[i][6] = String.valueOf(lv.getDatePublication());
				
				i++;
			}
			
			// Définir le modèle de la table avec le nouveau tableau.
			table.setModel(new ModeleTable(donnees, titreColonne));
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un probleme existe dans le systeme, " + "veuillez contacter\nl'administrateur du systeme",
					"Erreur systeme", JOptionPane.ERROR_MESSAGE);
			System.out.println(ex.getMessage());
			return;
		}
	}


}
