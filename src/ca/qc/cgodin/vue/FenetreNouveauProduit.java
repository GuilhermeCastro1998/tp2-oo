package ca.qc.cgodin.vue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// Demande a l'utilisateur s'il veut ajoute un film ou livre, apelle une fentre selon son choix
public class FenetreNouveauProduit extends JInternalFrame {
	
	public FenetreNouveauProduit() {
  
	    super("Nouveau produit", true, true, true, true);
	    setSize(700, 700);
	    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	    
	    JDesktopPane desktop = new JDesktopPane();
	    
	    
	    JButton btnLivre = new JButton("Livre");
	    JButton btnFilm = new JButton("Film");

	    JPanel panel = new JPanel(new GridBagLayout());
	    
	    GridBagConstraints gbc = new GridBagConstraints();

	    gbc.gridx = 0;
	    gbc.gridy = 0;
	    gbc.insets = new Insets(5, 5, 5, 5);
	    btnLivre.addActionListener(new ActionListener() {
	        
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            FenetreNouveauLivre f = new FenetreNouveauLivre();
	            f.setVisible(true);
	            desktop.add(f);
	        }
	        
	    });
	    panel.add(btnLivre, gbc);

	    gbc.gridx = 1;
	    btnFilm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				FenetreNouveauFilm f = new FenetreNouveauFilm();
	            f.setVisible(true);
	            desktop.add(f); 
			}
		});
	    panel.add(btnFilm, gbc);

	    add(panel, BorderLayout.NORTH); 

	    add(desktop, BorderLayout.CENTER);

	    setVisible(true);
	}
}
