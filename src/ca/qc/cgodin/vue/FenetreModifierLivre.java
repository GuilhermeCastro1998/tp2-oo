package ca.qc.cgodin.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import ca.qc.cgodin.modele.BiblioException;
import ca.qc.cgodin.modele.Client;
import ca.qc.cgodin.modele.Livre;

public class FenetreModifierLivre extends JInternalFrame {

	private JButton btnRechercher;

	private JTextField txtCode;
	private JTextField txtTitre;
	private JTextField txtImage;
	private JTextField txtPrixAchat;
	private JTextField txtPrixVente;
	private JTextField txtEtat;
	private JTextField txtUtilisation;
	private JTextField tfAuteur;
	private JTextField tfEditeur;
	private JTextField tfCollection;
	private JTextField tfLangue;
	private JTextField tfDatePublication;
	private JTextField tfISBN;
	private JButton cmdModifier = new JButton("Modifier");
	private JButton cmdRecommencer = new JButton("Recommencer");
	Livre l1;

//	Constructeur 
	public FenetreModifierLivre() {
		super("Modification Livre", true, true, true, true);
		setSize(500, 600);
		add(getPanelCentre());
		setVisible(true);
	}
// MEthode pour le contenu du centre
	private JPanel getPanelCentre() {
	    JPanel panel = new JPanel(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	    c.insets = new Insets(5, 5, 5, 5);
	    c.fill = GridBagConstraints.BOTH;

	    c.gridx = 0;
	    c.gridy = 0;
	    panel.add(new JLabel("Code"), c);

	    c.gridx = 1;
	    txtCode = new JTextField(15);
	    panel.add(txtCode, c);

	    c.gridx = 2;
	    JButton btnRechercher = new JButton("Rechercher");
	    btnRechercher.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			rechercherLivre();
				
			}
		});
	    panel.add(btnRechercher, c);

	    c.gridx = 0;
	    c.gridy = 1;
	    panel.add(new JLabel("Titre"), c);
	    c.gridx = 1;
	    txtTitre = new JTextField(15);
	    panel.add(txtTitre, c);

	    c.gridx = 0;
	    c.gridy = 2;
	    panel.add(new JLabel("Image"), c);
	    c.gridx = 1;
	    txtImage = new JTextField(15);
	    panel.add(txtImage, c);

	    c.gridx = 0;
	    c.gridy = 3;
	    panel.add(new JLabel("Prix d'Achat"), c);
	    c.gridx = 1;
	    txtPrixAchat = new JTextField(15);
	    panel.add(txtPrixAchat, c);

	    c.gridx = 0;
	    c.gridy = 4;
	    panel.add(new JLabel("Prix de Vente"), c);
	    c.gridx = 1;
	    txtPrixVente = new JTextField(15);
	    panel.add(txtPrixVente, c);

	    c.gridx = 0;
	    c.gridy = 5;
	    panel.add(new JLabel("Etat"), c);
	    c.gridx = 1;
	    txtEtat = new JTextField(15);
	    panel.add(txtEtat, c);

	    c.gridx = 0;
	    c.gridy = 6;
	    panel.add(new JLabel("Utilisation"), c);
	    c.gridx = 1;
	    txtUtilisation = new JTextField(15);
	    panel.add(txtUtilisation, c);

	    c.gridx = 0;
	    c.gridy = 7;
	    panel.add(new JLabel("Auteur"), c);
	    c.gridx = 1;
	    tfAuteur = new JTextField(15);
	    panel.add(tfAuteur, c);

	    c.gridx = 0;
	    c.gridy = 8;
	    panel.add(new JLabel("Editeur"), c);
	    c.gridx = 1;
	    tfEditeur = new JTextField(15);
	    panel.add(tfEditeur, c);

	    c.gridx = 0;
	    c.gridy = 9;
	    panel.add(new JLabel("Collection"), c);
	    c.gridx = 1;
	    tfCollection = new JTextField(15);
	    panel.add(tfCollection, c);

	    c.gridx = 0;
	    c.gridy = 10;
	    panel.add(new JLabel("Langue"), c);
	    c.gridx = 1;
	    tfLangue = new JTextField(15);
	    panel.add(tfLangue, c);

	    c.gridx = 0;
	    c.gridy = 11;
	    panel.add(new JLabel("DatePublication"), c);
	    c.gridx = 1;
	    tfDatePublication = new JTextField(15);
	    panel.add(tfDatePublication, c);

	    c.gridx = 0;
	    c.gridy = 12;
	    panel.add(new JLabel("ISBN"), c);
	    c.gridx = 1;
	    tfISBN = new JTextField(15);
	    panel.add(tfISBN, c);

	    c.gridy = 13;
	    c.gridx = 0;
	    cmdModifier.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				modifier();
			}
		});
	    panel.add(cmdModifier, c);

	    c.gridx = 1;
	    cmdRecommencer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				nettoyer();
			}
		});
	    panel.add(cmdRecommencer, c);

	    return panel;
	}

//	Recherche dans la table livre selon le code
	public void rechercherLivre() {
		String code = txtCode.getText();
		if (code.equals("")) {
			JOptionPane.showMessageDialog(this, "Il faut fournir le code du livre", "Information manquante",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			l1 = new Livre(code);
			txtTitre.setText(l1.getTitre());
			txtImage.setText(l1.getImage());
			txtPrixAchat.setText(Double.toString(l1.getPrixAchat()));
			txtPrixVente.setText(Double.toString(l1.getPrixVente()));
			txtEtat.setText(l1.getEtat());
			txtUtilisation.setText(l1.getUtilisation());
			tfAuteur.setText(l1.getAuteur());
			tfEditeur.setText(l1.getEditeur());
			tfCollection.setText(l1.getCollection());
			tfLangue.setText(l1.getLangue());
			tfDatePublication.setText(l1.getDatePublication());
			tfISBN.setText(l1.getIsbn());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un probleme existe dans le systeme, veuillez contacter" + "\nl'administrateur du systeme",
					"Erreur Systeme", JOptionPane.ERROR_MESSAGE);
			
			System.out.println(ex.getMessage());
		} catch (BiblioException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Livre non trouve", JOptionPane.WARNING_MESSAGE);
			return;
		}
	}

//	Modifie les infos du livre selon le input de l'utilisateur
	private void modifier() {
		String titre = txtTitre.getText();
		String image = txtImage.getText();
		String prixAchatStr = txtPrixAchat.getText();
		String prixVenteStr = txtPrixVente.getText();
		String etat = txtEtat.getText();
		String utilisation = txtUtilisation.getText();

		String auteur = tfAuteur.getText();
		String editeur = tfEditeur.getText();
		String collection = tfCollection.getText();
		String langue = tfLangue.getText();
		String datePublicationStr = tfDatePublication.getText();
		String isbn = tfISBN.getText();

		if (titre.equals("") || image.equals("") || prixAchatStr.equals("") || prixVenteStr.equals("") || etat.equals("")
				|| utilisation.equals("") || auteur.equals("") || editeur.equals("") || collection.equals("")
				|| langue.equals("") || datePublicationStr.equals("") || isbn.equals("")) {
			JOptionPane.showMessageDialog(this, "Il faut remplir le formulaire", "Information manquante",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		  // valider les input de l'utilisateur selon les contraintes de la bd
	    if (!(etat.equalsIgnoreCase("disponible") || etat.equalsIgnoreCase("emprunte"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Infio invalide pour \"Etat\". Il faut rentrer \"disponible\" ou \"emprunte\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	    if (!(utilisation.equalsIgnoreCase("a vendre") || utilisation.equalsIgnoreCase("a emprunte"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Information invalide pour \"utilisation\", veuillez ecrire  \"a vendre\" ou \"a emprunter\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	   
	    if (!(langue.equalsIgnoreCase("francais") || langue.equalsIgnoreCase("anglais"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Info invalide pour langue \"Langue\". veuillez ecrire  \"anglais\" ou \"francais\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
		try {
			double prixAchat = Double.parseDouble(prixAchatStr);
			double prixVente = Double.parseDouble(prixVenteStr);

			l1.setTitre(titre);
			l1.setImage(image);
			l1.setPrixAchat(prixAchat);
			l1.setPrixVente(prixVente);
			l1.setEtat(etat);
			l1.setUtilisation(utilisation);
			l1.setAuteur(auteur);
			l1.setEditeur(editeur);
			l1.setCollection(collection);
			l1.setLangue(langue);
			l1.setDatePublication(datePublicationStr);
			l1.setIsbn(isbn);

			l1.modifier();
			JOptionPane.showMessageDialog(this, "Le livre de code " + l1.getCode() + " vient d'être modifié",
					"Livre modifié", JOptionPane.PLAIN_MESSAGE);
			nettoyer();
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un problème existe dans le système, veuillez contacter" + "\nl'administrateur du système",
					"Erreur Système", JOptionPane.ERROR_MESSAGE);
			System.out.println(ex.getMessage());
			return;
		}
	}

	private void nettoyer() {
		txtCode.setText("");
		txtTitre.setText("");
		txtImage.setText("");
		txtPrixAchat.setText("");
		txtPrixVente.setText("");
		txtEtat.setText("");
		txtUtilisation.setText("");
		tfAuteur.setText("");
		tfEditeur.setText("");
		tfCollection.setText("");
		tfLangue.setText("");
		tfDatePublication.setText("");
		tfISBN.setText("");
	}
}


