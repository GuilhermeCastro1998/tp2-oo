package ca.qc.cgodin.vue;

import javax.swing.JInternalFrame;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.Client;
import ca.qc.cgodin.modele.BiblioException;
import ca.qc.cgodin.modele.Livre;
import ca.qc.cgodin.modele.Pret;
import ca.qc.cgodin.modele.Produit;
public class FenetreRecherClients extends JInternalFrame{

	private JTextField txtCodeClient;
	private	JTextField txtNom;
	private	JTextField txtPrenom;
	private	JTextField txtTel;
	private	JTextField txtAdresse;
	Client c1;
	
	String [] titreColonne = { "Titre du produit","Code du produit", "Date du pret"};
	String [][] donnees = new String [15][titreColonne.length];
	JTable table;
	public FenetreRecherClients() {
		super("Rechercher les prets du client", true,true,true,true);
		setSize(1000,900);
		JPanel contentPane = new JPanel();
		contentPane.add(getPanelCentre());
		add(contentPane);
		setVisible(true);
	}
//	Contenue du centre
	private JPanel getPanelCentre() {
	    JPanel panel = new JPanel(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	    c.insets = new Insets(5, 5, 5, 5);
	    c.fill = GridBagConstraints.HORIZONTAL;
	    
	    c.gridx = 0;
	    c.gridy = 0;
	    panel.add(new JLabel("Code du client"), c);
	    c.gridx = 1;
	    txtCodeClient = new JTextField(15);
	    panel.add(txtCodeClient, c);
	    c.gridx = 2;
	    JButton cmdRechercher = new JButton("Rechercher");
	    cmdRechercher.addActionListener(new ActionListener() {
	        @Override
	        public void actionPerformed(ActionEvent e) {
	            rechercherClient();
	        }
	    });
	    panel.add(cmdRechercher, c);
	    
	    c.gridx = 0;
	    c.gridy = 1;
	    panel.add(new JLabel("Nom"), c);
	    c.gridx = 1;
	    txtNom = new JTextField(15);
	    txtNom.setEditable(false);
	    panel.add(txtNom, c);
	    
	    c.gridx = 0;
	    c.gridy = 2;
	    panel.add(new JLabel("Prénom"), c);
	    c.gridx = 1;
	    txtPrenom = new JTextField(15);
	    txtPrenom.setEditable(false);
	    panel.add(txtPrenom, c);
	    
	    c.gridx = 0;
	    c.gridy = 3;
	    panel.add(new JLabel("Téléphone"), c);
	    c.gridx = 1;
	    txtTel = new JTextField(15);
	    txtTel.setEditable(false);
	    panel.add(txtTel, c);
	    
	    c.gridx = 0;
	    c.gridy = 4;
	    panel.add(new JLabel("Adresse"), c);
	    c.gridx = 1;
	    txtAdresse = new JTextField(15);
	    txtAdresse.setEditable(false);
	    panel.add(txtAdresse, c);

	    c.gridy = 5;
	    c.gridx = 0;
	    c.gridwidth = 3;
	    table = new JTable(new ModeleTable(donnees, titreColonne));

	    JScrollPane scrollPane = new JScrollPane(table);
	    scrollPane.setPreferredSize(new Dimension(900, 600));
	    panel.add(scrollPane, c);

	    return panel;
	}

//  Rechercher client selon le code
	public void rechercherClient() {
	    String code = txtCodeClient.getText();
	    if (code.equals("")) {
	        JOptionPane.showMessageDialog(this, "Il faut fournir le code du client", "Information manquante",
	                JOptionPane.WARNING_MESSAGE);
	        return;
	    }
	    try {
	        c1 = new Client(code);
	        txtNom.setText(c1.getNom());
	        txtPrenom.setText(c1.getPrenom());
	        txtAdresse.setText(c1.getAdresse());
	        txtTel.setText(c1.getTelephone());

	        List<Pret> prets = Pret.recupererListePret(code);
//	        Afficher les infos des prets
	        for (int i = 0; i < prets.size(); i++) {
	            Pret pret = prets.get(i);
	            String codeProduit = pret.getCodeProduit();
	            Date date = (Date) pret.getDatePret();
	            Produit produit = new Produit(codeProduit);
	            String titre = produit.getTitre();
	            donnees[i][0] = titre;
	            donnees[i][1] = codeProduit;
	            donnees[i][2] = date.toString();
	        }

	        table.setModel(new ModeleTable(donnees, titreColonne));
	        
	    } catch (SQLException ex) {
	        JOptionPane.showMessageDialog(this,
	                "Un probleme existe dans le systeme, veuillez contacter" + "\nl'administrateur du systeme",
	                "Erreur Systeme", JOptionPane.ERROR_MESSAGE);
	        System.out.println(ex.getMessage());
	    } catch (BiblioException ex) {
	        JOptionPane.showMessageDialog(this, ex.getMessage(), "Client non trouve", JOptionPane.WARNING_MESSAGE);
	        return;
	    }
	}

	
	
	
}
