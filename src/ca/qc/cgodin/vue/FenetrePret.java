package ca.qc.cgodin.vue;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.Client;
import ca.qc.cgodin.modele.DBManager;
import ca.qc.cgodin.modele.Produit;
import ca.qc.cgodin.modele.BiblioException;

public class FenetrePret extends JInternalFrame {
	
	JLabel labelCodeClient = new JLabel("Code Client:");
    JTextField txtFieldCodeClient = new JTextField(20);
    JLabel labelCodeProduit = new JLabel("Code Produit:");
    JTextField txtFieldCodeProduit = new JTextField(20);
    Client c1;
    Produit p1;
    

//    Constructeur 
 public FenetrePret() {
	// TODO Auto-generated constructor stub
	 super("Faire un pret");
	 setSize(500, 600);
		setClosable(true);
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		add(getPanelCentre());
		setVisible(true);
}
 
// Contenu du centre
 private JPanel getPanelCentre() {
	        JPanel panel = new JPanel(new GridBagLayout());
	        GridBagConstraints c = new GridBagConstraints();

	        

	   
	        JButton emprunterButton = new JButton("Emprunter");

	       emprunterButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				rechercherClient();
				rechercherProduit();
			try {
				emprunter();
			} catch (SQLException | BiblioException e1) {
				// TODO Auto-generated catch block
				System.out.println(e1.getMessage());
			}
			}
		});
	        
	        c.gridx = 0;
	        c.gridy = 0;
	        panel.add(labelCodeClient, c);

	        c.gridx = 1;
	        c.gridy = 0;
	        panel.add(txtFieldCodeClient, c);

	        c.insets = new Insets(10, 0, 0, 0);

	        c.gridx = 0;
	        c.gridy = 1;
	        panel.add(labelCodeProduit, c);

	        c.gridx = 1;
	        c.gridy = 1;
	        panel.add(txtFieldCodeProduit, c);

	        c.insets = new Insets(10, 0, 0, 0); 
	        c.gridx = 0;   
	        c.gridy = 2;  
	        c.gridwidth = 2; 
	        c.anchor = GridBagConstraints.CENTER;  
	        panel.add(emprunterButton, c);

	        return panel;
	    
 }
// recherche un client selon le code rentre par l'utilisateur
	public void rechercherClient() {
		String code = txtFieldCodeClient.getText();
		if (code.equals("")) {
			JOptionPane.showMessageDialog(this, "Il faut fournir le code du client", "Information manquante",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			c1 = new Client(code);
			
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un probleme existe dans le systeme, veuillez contacter" + "\nl'administrateur du systeme",
					"Erreur Systeme", JOptionPane.ERROR_MESSAGE);
			
			System.out.println(ex.getMessage());
		} catch (BiblioException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Client non trouve", JOptionPane.WARNING_MESSAGE);
			return;
		}

	}
	// recherche un produit selon le code rentre par l'utilisateur
	public void rechercherProduit() {
		String code = txtFieldCodeProduit.getText();
		if (code.equals("")) {
			JOptionPane.showMessageDialog(this, "Il faut fournir le code du produit", "Information manquante",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			p1 = new Produit(code);
			
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un probleme existe dans le systeme, veuillez contacter" + "\nl'administrateur du systeme",
					"Erreur Systeme", JOptionPane.ERROR_MESSAGE);
			
			System.out.println(ex.getMessage());
		} catch (BiblioException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Produit non trouve", JOptionPane.WARNING_MESSAGE);
			return;
		}

	}
//	Emprune le produit pour le client en question
	public void emprunter() throws SQLException, BiblioException {
		String requete = "SELECT COUNT(*) FROM Pret WHERE codeClient ='" + c1.getCode()+ "' AND dateRetour IS NULL";

	       ResultSet res = DBManager.executeQuery(requete);
	      
	       int count = 0;
	       if (res.next()) {
	           count = res.getInt(1); 
	       }

	       if (count >= 10) {
	        JOptionPane.showMessageDialog(this, "Ce client ne peut pas emprunter un autre produit, car il en a deja 10 prets");
	       } else {
	           
	    	   c1.preter(p1);
	   		JOptionPane.showMessageDialog(this, "Le produit a ete emprunte");
	   		dispose();
	       }
		
	}
	
}
