package ca.qc.cgodin.vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.Livre;

public class FenetreNouveauLivre extends JInternalFrame{
	
	
	private JLabel titreLbl= new JLabel("Titre");
	private JLabel imageLbl= new JLabel("Image");
	private JLabel prixAchatLbl= new JLabel("Prix d'achat");
	private JLabel prixVenteLbl= new JLabel("Prix de vente");
	private JLabel etatLbl= new JLabel("Etat");
	private JLabel utilisation= new JLabel("Utilisation");
	private JLabel labelAuteur = new JLabel("Auteur");
	private JTextField titreTxt = new JTextField(25);
	private JTextField imageTxt = new JTextField(25);
	private JTextField prixAchatTxt = new JTextField(25);
	private JTextField prixVenteTxt = new JTextField(25);
	private JTextField etatTxt = new JTextField(25);
	private JTextField utilTxt = new JTextField(25);
	
	private JTextField tfAuteur = new JTextField(25);
	private JLabel labelEditeur = new JLabel("Éditeur");
	private JTextField tfEditeur = new JTextField(25);
	private JLabel labelCollection = new JLabel("Collection");
	private JTextField tfCollection = new JTextField(25);
	private JLabel labelLangue = new JLabel("Langue");
	private JTextField tfLangue = new JTextField(25);
	private JLabel labelDateDePublication = new JLabel("Date de publication");
	private JTextField tfDateDePublication = new JTextField(25);
	private JLabel labelISBN = new JLabel("ISBN");
	private JTextField tfISBN = new JTextField(25);
	private JPanel imagePanel = new JPanel();
	
	private JPanel panelCardLayout;
	private CardLayout layout;
//	Constructeur
	public FenetreNouveauLivre() {
		super("Nouveau Livre");
		setSize(500, 600);
		setClosable(true);
		setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		add(getPanelCentre(), BorderLayout.CENTER);
		add(getPanelHaut(), BorderLayout.NORTH);
		  add(imagePanel, BorderLayout.SOUTH);
		setVisible(true);

	}
	
//	Contenue du centre
	public JPanel getPanelCentre() {
		
		panelCardLayout = new JPanel();
		layout = new CardLayout();
		panelCardLayout.setLayout(layout);

		// Card1
		JPanel card1 = new JPanel(new GridBagLayout());
		GridBagConstraints g = new GridBagConstraints();

		g.gridx = 0;
		g.gridy = 0;
		card1.add(new JLabel("Titre"), g);

		g.gridx = 1;
		g.gridy = 0;
		card1.add(titreTxt, g);

		g.gridx = 0;
		g.gridy = 1;
		card1.add(new JLabel("Image"), g);

		g.gridx = 1;
		g.gridy = 1;
		card1.add(imageTxt, g);

		g.gridx = 0;
		g.gridy = 2;
		card1.add(new JLabel("Prix d'achat"), g);

		g.gridx = 1;
		g.gridy = 2;
		card1.add(prixAchatTxt, g);

		g.gridx = 0;
		g.gridy = 3;
		card1.add(new JLabel("Prix de vente"), g);

		g.gridx = 1;
		g.gridy = 3;
		card1.add(prixVenteTxt, g);

		g.gridx = 0;
		g.gridy = 4;
		card1.add(new JLabel("Etat"), g);

		g.gridx = 1;
		g.gridy = 4;
		card1.add(etatTxt, g);

		g.gridx = 0;
		g.gridy = 5;
		card1.add(new JLabel("Utilisation"), g);

		g.gridx = 1;
		g.gridy = 5;
		card1.add(utilTxt, g);
		
		// Card2
		JPanel card2 = new JPanel(new GridBagLayout());

		Insets insets = new Insets(5, 10, 5, 10);

		g.insets = insets;

		g.gridx = 0;
		g.gridy = 1;
		card2.add(labelAuteur, g);

		g.gridx = 1;
		g.gridy = 1;
		card2.add(tfAuteur, g);

		g.gridx = 0;
		g.gridy = 2;
		card2.add(labelEditeur, g);

		g.gridx = 1;
		g.gridy = 2;
		card2.add(tfEditeur, g);

		g.gridx = 0;
		g.gridy = 3;
		card2.add(labelCollection, g);

		g.gridx = 1;
		g.gridy = 3;
		card2.add(tfCollection, g);

		g.gridx = 0;
		g.gridy = 4;
		card2.add(labelLangue, g);

		g.gridx = 1;
		g.gridy = 4;
		card2.add(tfLangue, g);

		g.gridx = 0;
		g.gridy = 5;
		card2.add(labelDateDePublication, g);

		g.gridx = 1;
		g.gridy = 5;
		card2.add(tfDateDePublication, g);

		g.gridx = 0;
		g.gridy = 6;
		card2.add(labelISBN, g);

		g.gridx = 1;
		g.gridy = 6;
		card2.add(tfISBN, g);

		g.insets = new Insets(0, 0, 0, 0);

		
		panelCardLayout.add(card1, "Card1");
		panelCardLayout.add(card2, "Card2");
		layout.show(panelCardLayout, "Card1");

		return panelCardLayout;
	}
	
	
//	Contenu du haut
	public JPanel getPanelHaut() {
		JPanel panelHaut = new JPanel();
		JButton cmdPrecedent = new JButton("Page 1");
		cmdPrecedent.setEnabled(false);
		JButton cmdSuivant = new JButton("Page 2");
		JButton cmdEnregistrer = new JButton("Enregistrer");
		JButton cmdAnnuler = new JButton("Annuler");
		panelHaut.add(cmdPrecedent);
		panelHaut.add(cmdSuivant);
		panelHaut.add(cmdEnregistrer);
		panelHaut.add(cmdAnnuler);

		cmdPrecedent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				layout.first(panelCardLayout);
				cmdPrecedent.setEnabled(false);
				cmdSuivant.setEnabled(true);

			}
		});
		cmdSuivant.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				layout.last(panelCardLayout);
				cmdPrecedent.setEnabled(true);
				cmdSuivant.setEnabled(false);
			}
		});
		cmdEnregistrer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				enregistrer();
			}
		});
		cmdAnnuler.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				nettoyer();
			}
		});
		return panelHaut;
	}
	

// Enregistrer les infos entrees par le client
	private void enregistrer() {
	    String titre = titreTxt.getText();
	    String image = imageTxt.getText();
	    String prixAchatStr = prixAchatTxt.getText();
	    String prixVenteStr = prixVenteTxt.getText();
	    String etat = etatTxt.getText();
	    String utilisation = utilTxt.getText();

	    String auteur = tfAuteur.getText();
	    String editeur = tfEditeur.getText();
	    String collection = tfCollection.getText();
	    String langue = tfLangue.getText();
	    String datePublicationStr = tfDateDePublication.getText();
	    String isbn = tfISBN.getText();
	    
//	    Verifie que les infos sont accetapbles selon les besoins de la BD
	    if (titre.isEmpty() || image.isEmpty() || etat.isEmpty() || utilisation.isEmpty() || auteur.isEmpty()
	            || editeur.isEmpty() || collection.isEmpty() || langue.isEmpty() || datePublicationStr.isEmpty()
	            || isbn.isEmpty() || prixAchatStr.isEmpty() || prixVenteStr.isEmpty()) {
	        JOptionPane.showMessageDialog(this, "Il faut remplir le formulaire", "Information manquante",
	                JOptionPane.WARNING_MESSAGE);
	        return;
	    }
	   
//	    if (!(etat.equalsIgnoreCase("disponible") || etat.equalsIgnoreCase("emprunte"))) {
//	        JOptionPane.showMessageDialog(this, 
//	              "Infio invalide pour \"Etat\". Il faut rentrer \"disponible\" ou \"emprunte\".",
//	              "Input Error",
//	              JOptionPane.ERROR_MESSAGE);
//	        return;
//	    }
	    if (utilisation.equalsIgnoreCase("a emprunte") && !prixVenteStr.equals("0")) {
	        JOptionPane.showMessageDialog(null, "Ce produit est a emprunter, le prix de vente doit être 0. Veuillez entrer 0 comme prix de vente.");
	        return; 
	    }
	    if (!etat.equalsIgnoreCase("disponible")) {
	        JOptionPane.showMessageDialog(this, 
	            "Pour enregistrer un livre, l'etat doit etre \"disponible\".",
	            "Input Error",
	            JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	    if (!(utilisation.equalsIgnoreCase("a vendre") || utilisation.equalsIgnoreCase("a emprunte"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Information invalide pour \"utilisation\", veuillez ecrire  \"a vendre\" ou \"a emprunte\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	   
	    if (!(langue.equalsIgnoreCase("francais") || langue.equalsIgnoreCase("anglais"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Info invalide pour langue \"Langue\". veuillez ecrire  \"anglais\" ou \"francais\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }

	    
	   
	    
	    try {
	        double prixAchat = Double.parseDouble(prixAchatStr);
	        double prixVente = Double.parseDouble(prixVenteStr);
	        
	        if (utilisation.equalsIgnoreCase("a vendre")) {
	        	if (prixVente != prixAchat * 0.3) {
		            JOptionPane.showMessageDialog(this, 
		                  "Le prix de vente doit être 30% du prix d'achat.", 
		                  "Erreur de saisie", 
		                  JOptionPane.ERROR_MESSAGE);
		            return;
		        }
			}
	        
	        // valider le format de la date pour SQL
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        sdf.setLenient(false); 
	        sdf.parse(datePublicationStr);  
	        
	        Livre l1 = new Livre(titre, image, prixAchat, prixVente, etat, utilisation, auteur, editeur, collection,
	                langue, datePublicationStr, isbn);  
	        l1.enregistrer();
	        JOptionPane.showMessageDialog(this,
	                "Un livre vient d'etre enregistre. \nSon code est : " + l1.getCode(), "Livre enregistre",
	                JOptionPane.PLAIN_MESSAGE);
	        nettoyer();
	    } catch (NumberFormatException e) {
	        JOptionPane.showMessageDialog(this,
	                "Assurez vous de rentrer un prix en chiffre.", "Erreur de saisie", JOptionPane.ERROR_MESSAGE);
	    } catch (ParseException e) {
	        JOptionPane.showMessageDialog(this,
	                "La date de publication doit être au format YYYY-MM-DD.", "Erreur de format de date",
	                JOptionPane.ERROR_MESSAGE);
	    } catch (SQLException ex) {
	        JOptionPane.showMessageDialog(this,
	                "Un probleme existe dans le systeme, veuillez contacter\nl'administrateur du systeme",
	                "Erreur Systeme", JOptionPane.ERROR_MESSAGE);
	        System.out.println(ex.getMessage());
	    }
//	    Afficher image
	    if (!image.isEmpty()) {
            try {
                BufferedImage originalImage = ImageIO.read(new File(image));
                ImageIcon imageIcon = new ImageIcon(originalImage);
                JLabel imageLabel = new JLabel(imageIcon);
                imagePanel.add(imageLabel);
                imagePanel.revalidate();
                imagePanel.repaint();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this, "l'image n'a pas pu etre charge", "Image Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}


	
	private void nettoyer() {

		titreTxt.setText("");
		imageTxt.setText("");
		prixAchatTxt.setText("");
		prixVenteTxt.setText("");
		etatTxt.setText("");
		utilTxt.setText("");
		tfAuteur.setText("");
		tfEditeur.setText("");
		tfCollection.setText("");
		tfLangue.setText("");
		tfDateDePublication.setText("");
		tfISBN.setText("");
	

	}
}
