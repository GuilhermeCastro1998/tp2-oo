package ca.qc.cgodin.vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ca.qc.cgodin.modele.BiblioException;
import ca.qc.cgodin.modele.Film;
import ca.qc.cgodin.modele.Livre;

public class FenetreModifierFilm extends JInternalFrame {

	private JTextField txtCode;
	private JTextField txtTitre;
	private JTextField txtImage;
	private JTextField txtPrixAchat;
	private JTextField txtPrixVente;
	private JTextField txtEtat;
	private JTextField txtUtilisation;
	private JTextField tfRealisateur;
	private JTextField tfStudio;
	private JTextField tfFormat;
	private JTextField tfActeurs;
	private JTextField tfLanguePisteSonore;
	private JButton cmdModifier = new JButton("Modifier");
	private JButton cmdRecommencer = new JButton("Recommencer");
	Film f1;

	public FenetreModifierFilm() {
		super("Modification Film", true, true, true, true);
		setSize(500, 600);
		add(getPanelCentre());
		setVisible(true);
	}
//	 contenu du centre
	private JPanel getPanelCentre() {
	    JPanel panel = new JPanel(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();
	    c.insets = new Insets(5, 5, 5, 5);
	    c.fill = GridBagConstraints.BOTH;

	    c.gridx = 0;
	    c.gridy = 0;
	    panel.add(new JLabel("Code"), c);

	    c.gridx = 1;
	    txtCode = new JTextField(15);
	    panel.add(txtCode, c);

	    c.gridx = 2;
	    JButton btnRechercher = new JButton("Rechercher");
	    btnRechercher.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			rechercherLivre();
				
			}
		});
	    panel.add(btnRechercher, c);

	    c.gridx = 0;
	    c.gridy = 1;
	    panel.add(new JLabel("Titre"), c);
	    c.gridx = 1;
	    txtTitre = new JTextField(15);
	    panel.add(txtTitre, c);

	    c.gridx = 0;
	    c.gridy = 2;
	    panel.add(new JLabel("Image"), c);
	    c.gridx = 1;
	    txtImage = new JTextField(15);
	    panel.add(txtImage, c);

	    c.gridx = 0;
	    c.gridy = 3;
	    panel.add(new JLabel("Prix d'Achat"), c);
	    c.gridx = 1;
	    txtPrixAchat = new JTextField(15);
	    panel.add(txtPrixAchat, c);

	    c.gridx = 0;
	    c.gridy = 4;
	    panel.add(new JLabel("Prix de Vente"), c);
	    c.gridx = 1;
	    txtPrixVente = new JTextField(15);
	    panel.add(txtPrixVente, c);

	    c.gridx = 0;
	    c.gridy = 5;
	    panel.add(new JLabel("Etat"), c);
	    c.gridx = 1;
	    txtEtat = new JTextField(15);
	    panel.add(txtEtat, c);

	    c.gridx = 0;
	    c.gridy = 6;
	    panel.add(new JLabel("Utilisation"), c);
	    c.gridx = 1;
	    txtUtilisation = new JTextField(15);
	    panel.add(txtUtilisation, c);

	    c.gridx = 0;
	    c.gridy = 7;
	    panel.add(new JLabel("Realisateur"), c);
	    c.gridx = 1;
	    tfRealisateur = new JTextField(15);
	    panel.add(tfRealisateur, c);

	    c.gridx = 0;
	    c.gridy = 8;
	    panel.add(new JLabel("Studio"), c);
	    c.gridx = 1;
	    tfStudio = new JTextField(15);
	    panel.add(tfStudio, c);

	    c.gridx = 0;
	    c.gridy = 9;
	    panel.add(new JLabel("Format"), c);
	    c.gridx = 1;
	    tfFormat = new JTextField(15);
	    panel.add(tfFormat, c);

	    c.gridx = 0;
	    c.gridy = 10;
	    panel.add(new JLabel("Acteurs"), c);
	    c.gridx = 1;
	    tfActeurs = new JTextField(15);
	    panel.add(tfActeurs, c);

	    c.gridx = 0;
	    c.gridy = 11;
	    panel.add(new JLabel("Langue piste sonore"), c);
	    c.gridx = 1;
	    tfLanguePisteSonore = new JTextField(15);
	    panel.add(tfLanguePisteSonore, c);

	    

	    c.gridy = 13;
	    c.gridx = 0;
	    cmdModifier.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				modifier();
			}
		});
	    panel.add(cmdModifier, c);

	    c.gridx = 1;
	    cmdRecommencer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				nettoyer();
			}
		});
	    panel.add(cmdRecommencer, c);

	    return panel;
	}
	
//	Cherche le livre selon le code
	public void rechercherLivre() {
		String code = txtCode.getText();
		if (code.equals("")) {
			JOptionPane.showMessageDialog(this, "Il faut fournir le code du livre", "Information manquante",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		try {
			f1 = new Film(code);
			txtTitre.setText(f1.getTitre());
			txtImage.setText(f1.getImage());
			txtPrixAchat.setText(Double.toString(f1.getPrixAchat()));
			txtPrixVente.setText(Double.toString(f1.getPrixVente()));
			txtEtat.setText(f1.getEtat());
			txtUtilisation.setText(f1.getUtilisation());
			tfRealisateur.setText(f1.getRealisateur());
			tfStudio.setText(f1.getStudio());
			tfFormat.setText(f1.getFormat());
			tfActeurs.setText(f1.getActeurs());
			tfLanguePisteSonore.setText(f1.getLanguePisteSonore());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un probleme existe dans le systeme, veuillez contacter" + "\nl'administrateur du systeme",
					"Erreur Systeme", JOptionPane.ERROR_MESSAGE);
		
			System.out.println(ex.getMessage());
		} catch (BiblioException ex) {
			JOptionPane.showMessageDialog(this, ex.getMessage(), "Film non trouve", JOptionPane.WARNING_MESSAGE);
			return;
		}
	}
	
//	Modifie le film selon les infos rentrees par l'utilisateur
	
	private void modifier() {
		String titre = txtTitre.getText();
		String image = txtImage.getText();
		String prixAchatStr = txtPrixAchat.getText();
		String prixVenteStr = txtPrixVente.getText();
		String etat = txtEtat.getText();
		String utilisation = txtUtilisation.getText();

	    String realisateur = tfRealisateur.getText();
	    String studio = tfStudio.getText();
	    String format = tfFormat.getText();
	    String acteurs = tfActeurs.getText();
	    String langue = tfLanguePisteSonore.getText();

		if (titre.equals("") || image.equals("") || prixAchatStr.equals("") || prixVenteStr.equals("") || etat.equals("")
				|| utilisation.equals("") || realisateur.equals("") || studio.equals("") || format.equals("")
				|| acteurs.equals("") || langue.equals("")) {
			JOptionPane.showMessageDialog(this, "Il faut remplir le formulaire, verifiez les deux pages", "Information manquante",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		 
	    if (!(etat.equalsIgnoreCase("disponible") || etat.equalsIgnoreCase("emprunte"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Infio invalide pour \"Etat\". Il faut rentrer \"disponible\" ou \"emprunte\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	    if (!(utilisation.equalsIgnoreCase("a vendre") || utilisation.equalsIgnoreCase("a emprunte"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Information invalide pour \"utilisation\", veuillez ecrire  \"a vendre\" ou \"a emprunter\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	 
	    if (!(format.equalsIgnoreCase("DVD") || format.equalsIgnoreCase("Blue Ray"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Info invalide pour \"format\". veuillez ecrire  \"DVD\" ou \"Blue Ray\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
	    
	   
	    if (!(langue.equalsIgnoreCase("francais") || langue.equalsIgnoreCase("anglais"))) {
	        JOptionPane.showMessageDialog(this, 
	              "Info invalide pour\"langue piste sonore\". veuillez ecrire  \"anglais\" ou \"francais\".",
	              "Input Error",
	              JOptionPane.ERROR_MESSAGE);
	        return;
	    }
		try {
			double prixAchat = Double.parseDouble(prixAchatStr);
			double prixVente = Double.parseDouble(prixVenteStr);

			f1.setTitre(titre);
			f1.setImage(image);
			f1.setPrixAchat(prixAchat);
			f1.setPrixVente(prixVente);
			f1.setEtat(etat);
			f1.setUtilisation(utilisation);
			f1.setRealisateur(realisateur);
			f1.setStudio(studio);
			f1.setFormat(format);
			f1.setLanguePisteSonore(langue);

			f1.modifier();
			JOptionPane.showMessageDialog(this, "Le livre de code " + f1.getCode() + " vient d'être modifié",
					"Livre modifié", JOptionPane.PLAIN_MESSAGE);
			nettoyer();
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(this,
					"Un problème existe dans le système, veuillez contacter" + "\nl'administrateur du système",
					"Erreur Système", JOptionPane.ERROR_MESSAGE);
			System.out.println(ex.getMessage());
			return;
		}
	}
	
	
	private void nettoyer() {
		txtCode.setText("");
		txtTitre.setText("");
		txtImage.setText("");
		txtPrixAchat.setText("");
		txtPrixVente.setText("");
		txtEtat.setText("");
		txtUtilisation.setText("");
		tfRealisateur.setText("");
		tfStudio.setText("");
		tfFormat.setText("");
		tfActeurs.setText("");
		tfLanguePisteSonore.setText("");
	}
}
