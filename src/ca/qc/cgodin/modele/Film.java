package ca.qc.cgodin.modele;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Film extends Produit {

	private String code;
	private String realisateur;
	private String studio;
	private String format;
	private String acteurs;
	private String languePisteSonore;

	
// creer un code pour un film
	public Film(String titre, String image, double prixAchat, double prixVente, String etat, String utilisation,
			String realisateur, String studio, String format, String langue, String datePublication)
			throws SQLException {
		super(titre, image, prixAchat, prixVente, etat, utilisation);

		this.realisateur = realisateur;
		this.studio = studio;
		this.format = format;
		this.acteurs = langue;
		this.languePisteSonore = datePublication;
		genererCode();
	}
	
//	recherche un livre dans la bd par son code

	public Film(String code) throws SQLException, BiblioException {
	    super();
	   
	    String requete = "select * from film join produit on film.code = produit.code where film.code ='" + code + "'";
	    ResultSet res = DBManager.executeQuery(requete);
	    boolean trouve = false;
	    while (res.next()) {
	        
	        setTitre(res.getString("titre"));
	        setImage(res.getString("image"));
	        setPrixAchat(res.getDouble("prixAchat"));
	        setPrixVente(res.getDouble("prixVente"));
	        setEtat(res.getString("etat"));
	        setUtilisation(res.getString("utilisation"));

	        this.code = res.getString("code");
	        realisateur = res.getString("realisateur");
	        studio = res.getString("studio");
	        format = res.getString("format");
	        acteurs = res.getString("acteurs");
	        languePisteSonore = res.getString("languePisteSonore");
	        
	        trouve = true;
	    }
	    if (!trouve) {
	        throw new BiblioException("Ce film n'existe pas");
	    }
	}

	
	// generer le code du livre selon le code maximum dans la base de donnee
	@Override
	public void genererCode() throws SQLException {
	    String requete = "SELECT MAX(CAST(SUBSTRING(code, 5) AS UNSIGNED)) AS max_code FROM Film";
	    ResultSet res = DBManager.executeQuery(requete);
	    int maxCode = 0;
	    while (res.next()) {
	        maxCode = res.getInt("max_code");
	    }
	    maxCode++;
	    if (maxCode < 10)
	        code = "FIL-000" + maxCode;
	    else if (maxCode < 100) {
	        code = "FIL-00" + maxCode;
	    } else if (maxCode < 1000) {
	        code = "FIL-0" + maxCode;
	    } else
	        code = "FIL-" + maxCode;

	    super.setCode(code);
	}
	
//	Enregistrer les infos du film dans la table film et produit de la bd

	public void enregistrer() throws SQLException {
		String requete2 = "insert into produit (code, titre, image, prixAchat, prixVente, etat, utilisation) values ('"
				+ code + "', '" + super.getTitre() + "', '" + super.getImage() + "', " + super.getPrixAchat() + ", "
				+ super.getPrixVente() + ", '" + super.getEtat() + "', '" + super.getUtilisation() + "')";
		DBManager.executeUpdate(requete2);
		String requete = "insert into film values ('" + code + "', '" + realisateur + "', '" + studio + "', '" + format
				+ "', '" + acteurs + "', '" + languePisteSonore + "')";
		DBManager.executeUpdate(requete);
	}
//	Supprimer les infos du film dans la table film, prets et produit de la bd
	public void supprimer() throws SQLException {
		String requete3 = "delete from pret where codeProduit='" + code + "'";
		DBManager.executeUpdate(requete3);
		String requete2 = "delete from film where code='" + code + "'";
		DBManager.executeUpdate(requete2);
		String requete = "delete from produit where code='" + code + "'";
		DBManager.executeUpdate(requete);
		
	}
	
//	Modifie les infos du film dans la table film et produit de la bd
	public void modifier() throws SQLException {
	    String requete = "UPDATE film SET realisateur ='" + realisateur + "', studio='" + studio + "', format='"
	            + format + "', acteurs='" + acteurs + "', languePisteSonore='" + languePisteSonore
	            + "' WHERE code='" + code + "'";

	    DBManager.executeUpdate(requete);
	    
	    String requete2 = "UPDATE produit SET titre ='" + getTitre() + "', image='" + getImage() + "', prixAchat=" 
	            + getPrixAchat() + ", prixVente=" + getPrixVente() + ", etat='" + getEtat() + "', utilisation='" 
	            + getUtilisation() + "' WHERE code='" + code + "'";
	            
	    DBManager.executeUpdate(requete2);
	}
	
//	Getters et settes

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getRealisateur() {
		return realisateur;
	}

	public void setRealisateur(String realisateur) {
		this.realisateur = realisateur;
	}

	public String getStudio() {
		return studio;
	}

	public void setStudio(String studio) {
		this.studio = studio;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getActeurs() {
		return acteurs;
	}

	public void setActeurs(String acteurs) {
		this.acteurs = acteurs;
	}

	public String getLanguePisteSonore() {
		return languePisteSonore;
	}

	public void setLanguePisteSonore(String languePisteSonore) {
		this.languePisteSonore = languePisteSonore;
	}

}
