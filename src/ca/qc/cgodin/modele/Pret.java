package ca.qc.cgodin.modele;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pret {
	private int id;
    private String codeClient;
    private String codeProduit;
    private Date datePret;
    private Date dateRetour;

    

public Pret(int id, String codeClient, String codeProduit, Date datePret, Date dateRetour) {
		super();
		this.id = id;
		this.codeClient = codeClient;
		this.codeProduit = codeProduit;
		this.datePret = datePret;
		this.dateRetour = dateRetour;
	}


// recuperer la liste de prets 
public static List<Pret> recupererListePret() throws SQLException {
    List<Pret> prets = new ArrayList<>();

    String query = "SELECT * FROM Pret";
    ResultSet resultSet = DBManager.executeQuery(query);

    while (resultSet.next()) {
        int id = resultSet.getInt("id");
        String codeClient = resultSet.getString("codeClient");
        String codeProduit = resultSet.getString("codeProduit");
        Date datePret = resultSet.getDate("datePret");
        Date dateRetour = resultSet.getDate("dateRetour");

        Pret pret = new Pret(id, codeClient, codeProduit, datePret, dateRetour);
        prets.add(pret);
    }

    return prets;
}

// recupere la liste de pret par le code du client
public static List<Pret> recupererListePret(String codeClient) throws SQLException {
    List<Pret> prets = new ArrayList<>();

    String query = "SELECT * FROM Pret WHERE codeClient ='" + codeClient + "' AND dateRetour IS NULL";
    ResultSet resultSet = DBManager.executeQuery(query);

    while (resultSet.next()) {
        int id = resultSet.getInt("id");
        String codeClientDB = resultSet.getString("codeClient");
        String codeProduit = resultSet.getString("codeProduit");
        Date datePret = resultSet.getDate("datePret");
        Date dateRetour = resultSet.getDate("dateRetour");

        Pret pret = new Pret(id, codeClientDB, codeProduit, datePret, dateRetour);
        prets.add(pret);
    }

    return prets;
}


public int getId() {
	return id;
}



public void setId(int id) {
	this.id = id;
}



public String getCodeClient() {
	return codeClient;
}



public void setCodeClient(String codeClient) {
	this.codeClient = codeClient;
}



public String getCodeProduit() {
	return codeProduit;
}



public void setCodeProduit(String codeProduit) {
	this.codeProduit = codeProduit;
}



public Date getDatePret() {
	return datePret;
}



public void setDatePret(Date datePret) {
	this.datePret = datePret;
}



public Date getDateRetour() {
	return dateRetour;
}



public void setDateRetour(Date dateRetour) {
	this.dateRetour = dateRetour;
}
}
