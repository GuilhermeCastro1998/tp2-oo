package ca.qc.cgodin.modele;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Produit  {
	private String code;
	private String titre;
	private String image;
	private double prixAchat;
	private double prixVente;
	private String etat;
	private String utilisation;

	public Produit(String titre, String image, double prixAchat, double prixVente, String etat, String utilisation)  {
		this.titre = titre;
		this.image = image;
		this.prixAchat = prixAchat;
		this.prixVente = prixVente;
		this.etat = etat;
		this.utilisation = utilisation;

	}

	
	
	public Produit() {
		super();
	}



	public Produit(String code, String titre, String image, double prixAchat, double prixVente, String etat,
			String utilisation) {
		this.code = code;
		this.titre = titre;
		this.image = image;
		this.prixAchat = prixAchat;
		this.prixVente = prixVente;
		this.etat = etat;
		this.utilisation = utilisation;

	}

//	Recherche un produit dans la BD selon un code
	public Produit(String code) throws SQLException, BiblioException {
		String requete = "select * from produit where code ='" + code + "'";
		ResultSet res = DBManager.executeQuery(requete);
		boolean trouve = false;
		while (res.next()) {
			this.code = res.getString("code");
			titre = res.getString("titre");
			image = res.getString("image");
			prixAchat = res.getDouble("prixAchat");
			prixVente = res.getDouble("prixVente");
			etat = res.getString("etat");
			utilisation = res.getString("utilisation");
			trouve = true;

		}
		if (!trouve) {
			throw new BiblioException("Ce produit n'existe pas");
		}
	}

	public void genererCode() throws SQLException {

	}

//	public void enregistrer() throws SQLException {
//		String requete = "insert into produit values ('" + code + "', '" + titre + "', '" + image + "', '" + prixAchat
//				+ "', '" + prixVente + "', '" + etat + ", " + "')";
//		DBManager.executeUpdate(requete);
//	}

//	public void supprimer() throws SQLException {
//		String requete = "delete from produit where code='" + code + "'";
//		DBManager.executeUpdate(requete);
//	}

//	Recupere la liste de produits
	public static ArrayList<Produit> recupererListeProduit() throws SQLException {
		ArrayList<Produit> liste = new ArrayList<Produit>();
		String requete = "select * from produit";
		ResultSet res = DBManager.executeQuery(requete);

		while (res.next()) {
			String code = res.getString("code");
			String titre = res.getString("titre");
			String image = res.getString("image");
			double prixAchat = res.getDouble("prixAchat");
			double prixVente = res.getDouble("prixVente");
			String etat = res.getString("etat");
			String utilisation = res.getString("utilisation");
			Produit p = new Produit(code, titre, image, prixAchat, prixVente, etat, utilisation);
			liste.add(p);
		}
		return liste;
	}

	
//	Getters et Setters
	public String toString() {
		return code + " " + titre + " " + image + " " + prixAchat + " " + prixVente + " " + etat + " " + utilisation;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrixAchat() {
		return prixAchat;
	}

	public void setPrixAchat(double prixAchat) {
		this.prixAchat = prixAchat;
	}

	public double getPrixVente() {
		return prixVente;
	}

	public void setPrixVente(double prixVente) {
		this.prixVente = prixVente;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getUtilisation() {
		return utilisation;
	}

	public void setUtilisation(String utilisation) {
		this.utilisation = utilisation;
	}

	
}
