package ca.qc.cgodin.modele;

public class BiblioException extends Exception {
	public BiblioException(String message) {
		super(message);
	}
}
