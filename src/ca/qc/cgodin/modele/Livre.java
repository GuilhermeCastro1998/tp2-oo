package ca.qc.cgodin.modele;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Livre extends Produit {

	private String code;
	private String auteur;
	private String editeur;
	private String collection;
	private String langue;
	private String datePublication;
	private String isbn;

	public Livre(String titre, String image, double prixAchat, double prixVente, String etat, String utilisation,
			String code, String auteur, String editeur, String collection, String langue, String datePublication,
			String isbn) {
		super(titre, image, prixAchat, prixVente, etat, utilisation);
		this.code = code;
		this.auteur = auteur;
		this.editeur = editeur;
		this.collection = collection;
		this.langue = langue;
		this.datePublication = datePublication;
		this.isbn = isbn;
	}



	public Livre(String titre, String image, double prixAchat, double prixVente, String etat, String utilisation) {
		super(titre, image, prixAchat, prixVente, etat, utilisation);
		// TODO Auto-generated constructor stub
	}

	public Livre(String code, String auteur, String editeur, String collection, String langue, String datePublication,
			String isbn) {
		this.code = code;
		this.auteur = auteur;
		this.editeur = editeur;
		this.collection = collection;
		this.langue = langue;
		this.datePublication = datePublication;
		this.isbn = isbn;
	}

//	Constructeur qui genere un code pour un livre
	public Livre(String titre, String image, double prixAchat, double prixVente, String etat, String utilisation,
			String auteur, String editeur, String collection, String langue, String datePublication, String isbn)
			throws SQLException {
		super(titre, image, prixAchat, prixVente, etat, utilisation);

		this.auteur = auteur;
		this.editeur = editeur;
		this.collection = collection;
		this.langue = langue;
		this.datePublication = datePublication;
		this.isbn = isbn;
		genererCode();
	}
	
//	Recherche les infos du livre dans la BD par son code

	public Livre(String code) throws SQLException, BiblioException {
	    super();
	   
	    String requete = "select * from livre join produit on livre.code = produit.code where livre.code ='" + code + "'";
	    ResultSet res = DBManager.executeQuery(requete);
	    boolean trouve = false;
	    while (res.next()) {
	        
	        setTitre(res.getString("titre"));
	        setImage(res.getString("image"));
	        setPrixAchat(res.getDouble("prixAchat"));
	        setPrixVente(res.getDouble("prixVente"));
	        setEtat(res.getString("etat"));
	        setUtilisation(res.getString("utilisation"));

	        this.code = res.getString("code");
	        auteur = res.getString("auteur");
	        editeur = res.getString("editeur");
	        collection = res.getString("collection");
	        langue = res.getString("langue");
	        datePublication = res.getString("datePublication");
	        isbn = res.getString("isbn");
	        trouve = true;
	    }
	    if (!trouve) {
	        throw new BiblioException("Ce livre n'existe pas");
	    }
	}

//	Generer un code le code max dans la table livre

	@Override
	public void genererCode() throws SQLException {
	    String requete = "SELECT MAX(CAST(SUBSTRING(code, 5) AS UNSIGNED)) AS max_code FROM Livre";
	    ResultSet res = DBManager.executeQuery(requete);
	    int maxCode = 0;
	    while (res.next()) {
	        maxCode = res.getInt("max_code");
	    }
	    maxCode++;
	    if (maxCode < 10)
	        code = "LIV-000" + maxCode;
	    else if (maxCode < 100) {
	        code = "LIV-00" + maxCode;
	    } else if (maxCode < 1000) {
	        code = "LIV-0" + maxCode;
	    } else
	        code = "LIV-" + maxCode;

	    super.setCode(code);
	}
// enregistrer un livre dans les tables produit et livre
	public void enregistrer() throws SQLException {
		String requete2 = "insert into produit (code, titre, image, prixAchat, prixVente, etat, utilisation) values ('"
				+ code + "', '" + super.getTitre() + "', '" + super.getImage() + "', " + super.getPrixAchat() + ", "
				+ super.getPrixVente() + ", '" + super.getEtat() + "', '" + super.getUtilisation() + "')";
		DBManager.executeUpdate(requete2);
		String requete = "insert into livre (code, auteur, editeur, collection, langue, datePublication, isbn) values ('"
				+ code + "', '" + auteur + "', '" + editeur + "', '" + collection + "', '" + langue + "', '"
				+ datePublication + "', '" + isbn + "')";
		DBManager.executeUpdate(requete);
	}
// supprimer un livre dans la BD des tables pret, livre et produit
	public void supprimer() throws SQLException {
	    String requete3 = "delete from pret where codeProduit='" + code + "'";
	    DBManager.executeUpdate(requete3);
	    String requete2 = "delete from livre where code='" + code + "'";
	    DBManager.executeUpdate(requete2);
	    String requete = "delete from produit where code='" + code + "'";
	    DBManager.executeUpdate(requete);
	}

// recuperer la liste de livres dans la BD
	public static ArrayList<Livre> recupererListeLivre() throws SQLException {
	    ArrayList<Livre> liste = new ArrayList<Livre>();
	    String requete = "select l.*, p.* from livre l join produit p on l.code = p.code";
	    ResultSet res = DBManager.executeQuery(requete);

	    while (res.next()) {
	        String code = res.getString("code");
	        String auteur = res.getString("auteur");
	        String editeur = res.getString("editeur");
	        String collection = res.getString("collection");
	        String langue = res.getString("langue");
	        String datePublication = res.getString("datePublication");
	        String isbn = res.getString("isbn");
	        String titre = res.getString("titre");
	        String image = res.getString("image");
	        double prixAchat = res.getDouble("prixAchat");
	        double prixVente = res.getDouble("prixVente");
	        String etat = res.getString("etat");
	        String utilisation = res.getString("utilisation");
	        
	        Livre l1 = new Livre(titre, image, prixAchat, prixVente, etat, utilisation, code, auteur, editeur, collection, langue, datePublication, isbn);
	        liste.add(l1);
	    }
	    return liste;
	}

//	Recuperer la liste de livres selon les criteres
	
	public static ArrayList<Livre> recupererListeLivre(HashMap<String, String> criteres) throws SQLException {
	    boolean deuxiemeCritere = false;
	    ArrayList<Livre> liste = new ArrayList<Livre>();
	    String requete = "select p.titre, p.image, p.prixAchat, p.prixVente, p.etat, p.utilisation, l.* from Livre l join Produit p on l.code = p.code";
	    
	    if (!criteres.isEmpty()) {
	        requete += " where ";
	        String auteur = criteres.get("auteur");
	        String editeur = criteres.get("editeur");
	        String collection = criteres.get("collection");
	        String langue = criteres.get("langue");
	        String datePublication = criteres.get("datePublication");
	        String isbn = criteres.get("isbn");
	        String code = criteres.get("code");
	        String titre = criteres.get("titre");
	        String image = criteres.get("image");
	        String prixAchat = criteres.get("prixAchat");
	        String prixVente = criteres.get("prixVente");
	        String etat = criteres.get("etat");
	        String utilisation = criteres.get("utilisation");

	        if (auteur != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.auteur='" + auteur + "'";
	            deuxiemeCritere = true;
	        }
	        if (editeur != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.editeur='" + editeur + "'";
	            deuxiemeCritere = true;
	        }
	        if (collection != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.collection='" + collection + "'";
	            deuxiemeCritere = true;
	        }
	        if (langue != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.langue='" + langue + "'";
	            deuxiemeCritere = true;
	        }
	        if (datePublication != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.datePublication='" + datePublication + "'";
	            deuxiemeCritere = true;
	        }
	        if (isbn != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.isbn='" + isbn + "'";
	            deuxiemeCritere = true;
	        }
	        if (code != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " l.code='" + code + "'";
	            deuxiemeCritere = true;
	        }
	        if (titre != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " p.titre='" + titre + "'";
	            deuxiemeCritere = true;
	        }
	        if (image != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " p.image='" + image + "'";
	            deuxiemeCritere = true;
	        }
	        if (prixAchat != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " p.prixAchat='" + prixAchat + "'";
	            deuxiemeCritere = true;
	        }
	        if (prixVente != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " p.prixVente='" + prixVente + "'";
	            deuxiemeCritere = true;
	        }
	        if (etat != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " p.etat='" + etat + "'";
	            deuxiemeCritere = true;
	        }
	        if (utilisation != null){
	            if (deuxiemeCritere)
	                requete += " and";
	            requete += " p.utilisation='" + utilisation + "'";
	            deuxiemeCritere = true;
	        }
	    }
	    
	    ResultSet res = DBManager.executeQuery(requete);

	    while (res.next()) {
	        String code = res.getString("code");
	        String titre = res.getString("titre");
	        String image = res.getString("image");
	        double prixAchat = res.getDouble("prixAchat");
	        double prixVente = res.getDouble("prixVente");
	        String etat = res.getString("etat");
	        String utilisation = res.getString("utilisation");
	        String auteur = res.getString("auteur");
	        String editeur = res.getString("editeur");
	        String collection = res.getString("collection");
	        String langue = res.getString("langue");
	        String datePublication = res.getString("datePublication");
	        String isbn = res.getString("isbn");

	        Livre l1 = new Livre(titre, image, prixAchat, prixVente, etat, utilisation, code, auteur, editeur, collection, langue, datePublication, isbn);
	        liste.add(l1);
	    }
	    return liste;
	}

//	Modifie les infos du livre dans la bd aux tables produit et livre
	 public void modifier() throws SQLException {
		    String requete = "UPDATE livre SET auteur ='" + auteur + "', editeur='" + editeur + "', collection='"
		            + collection + "', langue='" + langue + "', datePublication='" + datePublication + "', isbn='" + isbn 
		            + "' WHERE code='" + code + "'";

		    DBManager.executeUpdate(requete);
		    
		    String requete2 = "UPDATE produit SET titre ='" + getTitre() + "', image='" + getImage() + "', prixAchat=" 
		            + getPrixAchat() + ", prixVente=" + getPrixVente() + ", etat='" + getEtat() + "', utilisation='" 
		            + getUtilisation() + "' WHERE code='" + code + "'";
		            
		    DBManager.executeUpdate(requete2);
		}
	 
//		Getters et settes
		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getAuteur() {
			return auteur;
		}

		public void setAuteur(String auteur) {
			this.auteur = auteur;
		}

		public String getEditeur() {
			return editeur;
		}

		public void setEditeur(String editeur) {
			this.editeur = editeur;
		}

		public String getCollection() {
			return collection;
		}

		public void setCollection(String collection) {
			this.collection = collection;
		}

		public String getLangue() {
			return langue;
		}

		public void setLangue(String langue) {
			this.langue = langue;
		}

		public String getDatePublication() {
			return datePublication;
		}

		public void setDatePublication(String datePublication) {
			this.datePublication = datePublication;
		}

		public String getIsbn() {
			return isbn;
		}

		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}

}
