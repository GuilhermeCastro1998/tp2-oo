package ca.qc.cgodin.modele;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Client {
	private String code;
	private String nom;
	private String prenom;
	private String adresse;
	private String telephone;
	
	// Constructeur (génère le code client automatiquement)

	public Client(String nom, String prenom, String adresse, String telephone) throws SQLException {
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.telephone = telephone;
		genererCode();
	}

	public Client(String code, String nom, String prenom, String adresse, String telephone) {
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		this.telephone = telephone;
	}
	
//	Constructeur qui recupere les informations du client par son code dans la Base de donnee

	public Client(String code) throws SQLException, BiblioException {
		String requete = "select * from client where code ='" + code + "'";
		ResultSet res = DBManager.executeQuery(requete);
		boolean trouve = false;
		while (res.next()) {
			this.code = res.getString("code");
			nom = res.getString("nom");
			prenom = res.getString("prenom");
			adresse = res.getString("adresse");
			telephone = res.getString("telephone");
			trouve = true;

		}
		if (!trouve) {
			throw new BiblioException("Ce client n'existe pas");
		}
	}
	
//	generer un code selon le nombre de clients dans la base de donnees 

	public void genererCode() throws SQLException {
		String requete = "SELECT COUNT(*) FROM Client";
		ResultSet res = DBManager.executeQuery(requete);
		int nombre = 0;
		while (res.next()) {
			nombre = res.getInt(1);
		}
		nombre++;
		if (nombre < 10)
			code = "BIBL-0000" + nombre;
		else if (nombre < 100) {
			code = "BIBL-000" + nombre;
		} else if (nombre < 1000) {
			code = "BIBL-00" + nombre;
		} else if (nombre < 10000) {
			code = "BIBL-0" + nombre;
		} else
			code = "BIBL-" + nombre;
	}
	
//	Enregistrer un client dans la base de donnee

	public void enregistrer() throws SQLException {
		String requete = "INSERT INTO client (code, nom, prenom, adresse, telephone) VALUES ('" + code + "', '" + nom
				+ "', '" + prenom + "', '" + adresse + "', '" + telephone + "')";
		DBManager.executeUpdate(requete);
	}
//	Modifie un client dans la base de donnee
	public void modifier() throws SQLException {
		String requete = "update client set nom ='" + nom + "', prenom='" + prenom + "', telephone='" + telephone
				+ "', adresse='" + adresse + "' where code='" + code + "'";
		DBManager.executeUpdate(requete);
	}

//	Recupere la liste de clients dans la base de donnee
	public static ArrayList<Client> recupererListeClient() throws SQLException {
		ArrayList<Client> liste = new ArrayList<Client>();
		String requete = "select * from client";
		ResultSet res = DBManager.executeQuery(requete);

		while (res.next()) {
			String code = res.getString("code");
			String nom = res.getString("nom");
			String prenom = res.getString("prenom");
			String adresse = res.getString("adresse");
			String telephone = res.getString("telephone");
			Client cl = new Client(code, nom, prenom, adresse, telephone);
			liste.add(cl);
		}
		return liste;
	}
	
//	Verifie que le code du client a moins que 10 prets dans la base de donnees, si oui ajoute un pret avec le code du client et du produit
	public void preter(Produit produit) throws SQLException, BiblioException {
	    String requete = "SELECT COUNT(*) FROM Pret WHERE codeClient ='" + this.code+ "' AND dateRetour IS NULL";

	    ResultSet res = DBManager.executeQuery(requete);

	    int count = 0;
	    if (res.next()) {
	        count = res.getInt(1); 
	    }

	    if (count >= 10) {
	        throw new BiblioException("Ce client ne peut prendre plus que 10 prets");
	    } else {

	        String requeteProduit = "SELECT etat, utilisation FROM produit WHERE code = '" + produit.getCode() + "'";
	        ResultSet resProduit = DBManager.executeQuery(requeteProduit);
	        
	        String etat = "";
	        String utilisation = "";
	        if (resProduit.next()) {
	            etat = resProduit.getString("etat");
	            utilisation = resProduit.getString("utilisation");
	        }

	        // Verifie que le produit est a preter et n'est pas deja prete
	        if (!etat.equals("disponible")) {
	        	JOptionPane.showMessageDialog(null, "Ce produit n'est pas disponible actuellement ","ERROR", JOptionPane.ERROR_MESSAGE);
	            throw new BiblioException("Ce produit n'est pas disponible");
	        } else if (!utilisation.equals("a emprunte")) {
	            
	            JOptionPane.showMessageDialog(null, "Ce produit est a vendre et non a emprunter","ERROR", JOptionPane.ERROR_MESSAGE);
	            throw new BiblioException("Ce produit est a vendre et non a emprunter"); 
	        } else {
	            String formattedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	            String requete2 = "INSERT INTO Pret (codeClient, codeProduit, datePret) VALUES ('" + this.code + "', '" + produit.getCode() + "', '" + formattedDate + "')";
	            DBManager.executeUpdate(requete2);
	            
//	            Update l'etat pour emprunte
	            String requete3 = "UPDATE produit SET etat='emprunte' WHERE code='" + produit.getCode() + "'";
	            DBManager.executeUpdate(requete3);
	        }
	    }
	}


//	    Affiche les prets du client
	    public void afficherPrets() throws SQLException {
	        String requete = "SELECT pr.codeProduit, pr.datePret " +
	                         "FROM Pret pr " +
	                         "WHERE pr.codeClient ='" + this.code + "' AND pr.dateRetour IS NULL";
	        ResultSet res = DBManager.executeQuery(requete);

	        while (res.next()) {
	            String codeProduit = res.getString("codeProduit");
	            Date date = res.getDate("datePret");
	        }
	    }


	

// getters et setters

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}



}
